//import java.util.Properties;	
import java.util.*;

import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;

public class PropertiesEx extends Properties {
    public void load(FileInputStream fis) throws IOException {
        Scanner in = new Scanner(fis);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        while(in.hasNext()) {
            out.write(in.nextLine().replace("\\","\\\\").getBytes());
            out.write("\n".getBytes());
        }

        InputStream is = new ByteArrayInputStream(out.toByteArray());
        super.load(is);
    }
	public void store(OutputStream out, String header) throws IOException
	{
		// The spec says that the file must be encoded using ISO-8859-1.
		PrintWriter writer 	= new PrintWriter(new OutputStreamWriter(out, "ISO-8859-1"));
      
		Iterator iter = entrySet ().iterator ();
		int i = size ();
		StringBuilder s = new StringBuilder (); // Reuse the same buffer.
		while (--i >= 0)
			{
				Map.Entry entry = (Map.Entry) iter.next ();
				formatForOutput ((String) entry.getKey (), s, true);
				s.append ('=');
				formatForOutput ((String) entry.getValue (), s, false);
				writer.println (s);
			}
  
		writer.flush ();
	}
	// @Override
	private void formatForOutput(String str, StringBuilder buffer, boolean key)
	{
		if (key)
			{
				buffer.setLength(0);
				buffer.ensureCapacity(str.length());
			}
		else
			buffer.ensureCapacity(buffer.length() + str.length());
		boolean head = true;
		int size = str.length();
		for (int i = 0; i < size; i++)
			{
				char c = str.charAt(i);
				switch (c)
					{
					case '\n':
						buffer.append("\\n");
						break;
					case '\r':
						buffer.append("\\r");
						break;
					case '\t':
						buffer.append("\\t");
						break;
					case ' ':
						buffer.append(head ? "\\ " : " ");
						break;
					case '\\':
					case '!':
					case '#':
					case '=':
					case ':':
						//						buffer.append('\\').append(c);
						//break;
					default:
						if (c < ' ' || c > '~')
							{
								String hex = Integer.toHexString(c);
								buffer.append("\\u0000".substring(0, 6 - hex.length()));
								buffer.append(hex);
							}
						else
							buffer.append(c);
					}
				if (c != ' ')
					head = key;
			}
	}
}
