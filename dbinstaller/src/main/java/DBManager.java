import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;

//import java.util.Properties;


import java.io.IOException;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

class DBManager {
	String host = "localhost";
	String dbname;
	Integer port = 3306;
	String username;
	String password;
	Connection conn = null;
	public DBManager(String host, String dbname, Integer port, String username, String password){
		this.host = host;
		this.dbname = dbname;
		this.port = port;
		this.username = username;
		this.password = password;
	}
	public DBManager(String host, String dbname,  String username, String password){
		this.host = host;
		this.dbname = dbname;
		this.username = username;
		this.password = password;
	}
	public DBManager(Integer port, String dbname, String username, String password){
		this.port = port;
		this.dbname = dbname;
		this.username = username;
		this.password = password;
	}
	public DBManager(String dbname, String username, String password){
		this.dbname = dbname;
		this.username = username;
		this.password = password;
	}
	public Boolean setConnection(){
		try {
            // The newInstance() call is a work around for some
            // broken Java implementations
			Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		try {
			conn = DriverManager.getConnection("jdbc:mysql://" + host + ":"+ port.toString() +"/"+ dbname +"?user="+ username +"&password=" + password);
		} catch (SQLException ex) {
			return false;
		}
		return true;
	}
	public Boolean updateCharset(){
		Statement stmt = null;
		try {
			if(conn.isClosed()){
				System.out.println("ERROR: No database connection. Unable to update database charset");
				return false;
			}
			stmt = conn.createStatement();
		}catch(Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		
		try {
			stmt.executeUpdate("ALTER DATABASE `" + dbname + "` DEFAULT CHARACTER SET utf8;");
			stmt.executeUpdate("ALTER DATABASE `" + dbname + "` DEFAULT COLLATE utf8_general_ci;");
		}catch (Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		return true;
	}
	//http://stackoverflow.com/questions/10699055/how-to-escape-colon-in-properties-file
	//http://developer.classpath.org/doc/java/util/Properties-source.html
	public Boolean setConfiguration(){
		PropertiesEx props = new PropertiesEx();
		OutputStream propOut;
		InputStream propIn;
		try {
			propIn = new FileInputStream(new File("/usr/local/multicabinet2/etc/multicabinet2.properties"));
		}catch (Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		
		try {
			props.load(propIn);
		}catch(Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		try {
			propOut = new FileOutputStream(new File("/usr/local/multicabinet2/etc/multicabinet2.properties"));
		}catch (Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		
		props.setProperty("dataSource.username", username);
		props.setProperty("dataSource.password", password);
		props.setProperty("dataSource.url", "jdbc:mysql://" + host + ":" + port + "/" + dbname + "?useUnicode=yes&characterEncoding=UTF-8&autoReconnect=true");
		try {
			props.store(propOut, "Database Processor Properties");
		}catch(Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		return true;
	}
}
