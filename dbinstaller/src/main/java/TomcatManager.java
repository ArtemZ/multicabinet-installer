import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
class TomcatManager {
	public static void updateConfig(String filepath, String domain) {  
 
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);
 
			// Get the root element
			Node server = doc.getFirstChild();
 
			// Get the staff element , it may not working if tag has spaces, or
			// whatever weird characters in front...it's better to use
			// getElementsByTagName() to get it directly.
			// Node staff = company.getFirstChild();
 
			// Get the staff element by tag name directly
			Node engine  = doc.getElementsByTagName("Engine").item(0);
			NodeList virtualHosts = engine.getChildNodes();
			
			// update engine attribute
			NamedNodeMap attr = engine.getAttributes();
			Node nodeAttr = attr.getNamedItem("defaultHost");
			nodeAttr.setTextContent(domain);
 
			NodeList listHosts = engine.getChildNodes();
			Boolean insertHost = true;
			for (int i = 0; i < listHosts.getLength(); i++) {
 
				Node node = listHosts.item(i);
 
				// get the salary element, and update the value
				if ("Host".equals(node.getNodeName())) {
					NamedNodeMap nodeAttributes = node.getAttributes();
				    Node hostNodeAttr = nodeAttributes.getNamedItem("appBase");
				    if("multicabinet".equals(hostNodeAttr.getNodeValue())){
				        insertHost = false;
				    }
				}
 
			}

			// append a new node to staff
			if(insertHost){
				Element host = doc.createElement("Host");
				host.setAttribute("name", domain);
				host.setAttribute("appBase", "multicabinet");
				host.setAttribute("unpackWARs", "true");
				host.setAttribute("autoDeploy", "true");
				host.setAttribute("xmlValidation", "false");
				host.setAttribute("xmlNamespaceAware", "false");
				engine.insertBefore(host, virtualHosts.item(0));
			}
			// loop the staff child node
			/*
			  NodeList listHosts = eninge.getChildNodes();
			Boolean insertHost = true;
			for (int i = 0; i < listHost.getLength(); i++) {
 
				Node node = listHosts.item(i);
 
				// get the salary element, and update the value
				if ("Host".equals(node.getNodeName())) {
				    nodeAttributes = node.getAttributes();
				    Node hostNodeAttr = nodeAttributes.getNamedItem("appBase");
				    if("multicabinet".equals(hostNodeAttr.getNodeValue())){
				        insertHost = false;
				    }
				}
 
				//remove firstname
				if ("firstname".equals(node.getNodeName())) {
					staff.removeChild(node);
				}
 
			}
			*/
			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //intendation
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);
 
			System.out.println("Done");
 
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
	}
}
