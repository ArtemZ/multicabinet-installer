import java.util.Scanner;
import java.net.InetAddress;
class Main {
	DBManager db;
	Scanner scan;
	public static String tomcatConfigPath = "";
	public static void main(String[] args){
		new Main();
		/*DBManager db = new DBManager();
		if(!db.setConnection()){
			
		}
		*/
		/*try {
			System.out.println("Inet address: " + InetAddress.getLocalHost().getHostAddress());
		}catch(java.net.UnknownHostException e){
		}
		tomcatConfigPath = "/home/artemz/Documents/multicabinet-installer/server.xml";
		//		TomcatManager.updateConfig("/home/artemz/Documents/multicabinet-installer/server.xml", "test.com");
		*/
	}
	public Main(){
		scan = new Scanner(System.in);
		Boolean connected = false;
		while(!connected) {
			setupDb();
			if(!db.setConnection() || !db.updateCharset()){
				System.out.println("Unable to setup database connection. Please, try again");
			} else {
				connected = true;
			}
		}
		if(!db.setConfiguration()){
			System.out.println("Unable to update database configuration");
		}
	}
	public void setupDb(){
		System.out.print("DB Host (default: localhost): ");
		String host = scan.nextLine();
		Integer port = readIntValue("DB Port (default: 3306): ", 3306);
		String dbname = readValue("DB Name: ");
		String username = readValue("DB User: ");
		String password = readValue("DB Password: ");
		if(host == null && port == null){
			db = new DBManager(dbname, username, password);
		} else if(host == null){
			db = new DBManager(port, dbname, username, password);
		} else if(port == null){
			db = new DBManager(host, dbname, username, password);
		} else {
			db = new DBManager(host, dbname, port, username, password);
		}
		
	}
	public boolean setupTomcat(String filepath){
		String defHostAddr;
		try {
			defHostAddr = InetAddress.getLocalHost().getHostAddress();
		}catch(Exception ex){
			System.out.println("ERROR: " + ex.getMessage());
			return false;
		}
		System.out.print("Billing domain name (default : "  + defHostAddr + "):");
		String host = scan.nextLine();
		if(host.length() == 0){
			host = defHostAddr;
		}
		TomcatManager.updateConfig(filepath, host);
		return true;
	}
	public String readValue(String valueName){
		String value = "";
		int count = 0;
		while(value.length() < 1){
			if(count > 0){
				System.out.println("Incorrect value specified. Please, try again");
			}
			count += 1;
			System.out.print(valueName);
			value = scan.nextLine();
		}
		return value;
	}
	public int readIntValue(String valueName, Integer defaultValue){
		int value = 0;
		int count = 0;
		while(value == 0){
			if(count > 0){
				System.out.println("Incorrect value specified. Please, try again");
			}
			count += 1;
			System.out.print(valueName);
			String receivedData = scan.nextLine();
			if(receivedData.length() == 0){
				return defaultValue;
			}
			try {
				value = Integer.parseInt(receivedData);
			}catch(NumberFormatException e){
				System.out.println(e.getMessage());
				value = 0;
			}
		}
		return value;
	}

}
