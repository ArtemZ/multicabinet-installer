#!/bin/sh

#publically accessible directory
WWW_DIR=/var/www/netdedicated/data/www/files.develdynamic.com
REPO=git@bitbucket.org:ArtemZ/multicabinet-installer.git
#git binary path
GIT=`which git 2>/dev/null`
if [ ! -x $WWW_DIR ]; then
	echo "Public directory do not exist"
	exit 1
fi
if [ ! -x $WWW_DIR/multicabinet-installer ]; then
	#TODO: create and populate multicabinet-installer dir
	echo "multicabinet-installer dir was not found, creating it"
	mkdir -p $WWW_DIR && cd $WWW_DIR
	$GIT clone $REPO
	#exit 1;
fi
cd $WWW_DIR/multicabinet-installer && $GIT pull
tar pczf $WWW_DIR/multicabinet2.tar.gz multicabinet2
cp $WWW_DIR/multicabinet-installer/dbinstaller/build/libs/dbinstaller-1.0.jar $WWW_DIR/dbinstaller-1.0.jar
