#!/sbin/sh
set -e
WAR_PATH=/home/artemz/Documents/multicabinet/target/multicabinet-0.1.war
TMP_DIR=/tmp
REMOTE_DIR=/var/www/netdedicated/data/www/files.develdynamic.com
SERVER=dns.netdedicated.ru

echo "Moving file to temprary dir..."
cp $WAR_PATH $TMP_DIR/ROOT.war
echo "Done."
echo "Doing checksum..."
md5sum $TMP_DIR/ROOT.war > $TMP_DIR/ROOT.war.md5
echo "Done."
echo "Uploading war to remote server..."
rsync -avhe ssh $TMP_DIR/ROOT.war $SERVER:$REMOTE_DIR/ROOT.war
echo "Done."
echo "Uploading checksum to remote server..."
rsync -avhe ssh $TMP_DIR/ROOT.war.md5 $SERVER:$REMOTE_DIR/ROOT.war.md5
echo "Done."
echo "Removing temrary data..."
rm -f $TMP_DIR/ROOT.war.md5 $TMP_DIR/ROOT.war
echo "Done."
echo "Complete"
