#!/bin/sh
DPKG_QUERY=`which dpkg-query`
INIT_DIR="/etc/init.d"
RC_D="which update-rc.d"
JAVA_PATH=`which java`
#test=( `dpkg-query -W -f='${Package}\n'  'openjdk-*-jre'`); test2=${#test[@]}; expr $test2 - 1;
if [ `id -u` -ne 0 ]; then
	echo "You need root privileges to run this script"
	exit 1
fi
if [ ! -d "$INIT_DIR" ]; then
	echo "Could not find init directory"
	exit 1
fi
if [ ! -x "$RC_D" ]; then
	echo "Could not find update-rc.d programm or it is not executable"
	exit 1
fi
#installing latest openjdk if no java setup found
if [! -x "$JAVA_PATH"]; then
	pkgarray=( `dpkg-query -W -f='${Package}\n'  'openjdk-*-jre'`);
	pkgcount=${#pkgarray[@]}
	apt-get -y install ${pkgarray[`expr $pkgcount - 1`]}
fi
#database configuration
wget http://files.develdynamic.com/multicabinet-installer/dbinstaller-1.0.jar -O /tmp/dbinstaller.jar
$JAVA_PATH -jar /tmp/dbinstaller.jar

#init scripts
wget http://files.develdynamic.com/multicabinet-installer/distro/debian-init.sh -O $INIT_DIR/multicabinet
$RC_D multicabinet defaults



echo "Starting multicabinet..."
$INIT_DIR/multicabinet start
echo "************* SUCCESS ************* \
Multicabinet has been successfully installed! \
You can sign in using default username and password at \
http://127.0.0.1:8080"
return 0
