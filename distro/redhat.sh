#!/bin/sh
INIT_DIR="/etc/init.d"
RC_D=`which --skip-alias chkconfig 2>/dev/null`
JAVA_PATH=`which --skip-alias java 2>/dev/null`
MULTICABINET_USER=multicabinet
set -e
#test=( `dpkg-query -W -f='${Package}\n'  'openjdk-*-jre'`); test2=${#test[@]}; expr $test2 - 1;
if [ `id -u` -ne 0 ]; then
	echo "You need root privileges to run this script"
	exit 1
fi
if [ ! -d "$INIT_DIR" ]; then
	echo "Could not find init directory"
	exit 1
fi
if [ ! -x "$RC_D" ]; then
	echo "Could not find chkconfig programm or it is not executable"
	exit 1
fi
#installing latest openjdk if no java setup found
if [ ! -x "$JAVA_PATH" ]; then
	pkgarray=( `yum list | awk '$1 ~ /^java-[0-9.]+-openjdk\..*$/ { print $1 }'`);
	pkgcount=${#pkgarray[@]}
	yum -y install ${pkgarray[`expr $pkgcount - 1`]}
	set +e
	JAVA_PATH=`which --skip-alias java 2>/dev/null`
	set -e
fi
if [ ! -x "$JAVA_PATH" ]; then
	echo "No java found. Unable to automatically install and detect of java executable"
	exit 1
fi
#installing other needed packages
yum -y install redhat-lsb
#adding multicabinet user
if id -u $MULTICABINET_USER > /dev/null 2>&1; then
	echo "User $MULTICABINET_USER already exists"
else
	adduser $MULTICABINET_USER
fi
chown -R $MULTICABINET_USER.$MULTICABINET_USER /usr/local/multicabinet2
#database configuration
wget http://files.develdynamic.com/multicabinet-installer/dbinstaller-1.0.jar -O /tmp/dbinstaller.jar
$JAVA_PATH -jar /tmp/dbinstaller.jar && rm -f /tmp/dbinstaller.jar

#init scripts
wget http://files.develdynamic.com/multicabinet-installer/distro/redhat-init.sh -O $INIT_DIR/multicabinet
chmod 755 $INIT_DIR/multicabinet
$RC_D multicabinet on

echo "Starting multicabinet..."
$INIT_DIR/multicabinet start
echo "************* SUCCESS *************"
echo "Multicabinet has been successfully installed!"
echo "You can sign in using default username and password at"
echo "http://127.0.0.1:8080"
exit 0
